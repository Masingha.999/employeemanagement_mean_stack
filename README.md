
# EmployeeManagement System Using Nodejs, MongoDB - With JWT authentication 

In this project I try to cover
- Only registerd users (user and admin) can access. 
- User can create (add employees) , view details , update details , delete details.
- Configuration Local mongoDB database.
- User can Signup and Sign in. (This function will secure using JWT token and session handling.)
                        Note - Session will expires after 24 hours.
- Admin can view how many employees works in 4 departments.


## Project Setup.(create nodemodules)

To make it easy for you to run 
        `npm install`

## Run It

We can run it in localhost server using
        `node server`   
and using 
        http://localhost:4200

# Below we can see how to works functions and DB records

## How to Signup Function works

![Sign up Function](https://gitlab.com/Masingha.999/employeemanagement_mean_stack/-/raw/main/snapps/Register_User-01.PNG "Optional title")

## How to SignIn Function works

![Sign IN Function](https://gitlab.com/Masingha.999/employeemanagement_mean_stack/-/raw/main/snapps/User_SignIn_Test.PNG "Optional title")

## DB Looks after sucessfully Signup.

![DB after SignUp](https://gitlab.com/Masingha.999/employeemanagement_mean_stack/-/raw/main/snapps/Userwith_MongoDB_AfterRegistration.PNG "Optional title")

## Create Employee Function 

![Create Employee](https://gitlab.com/Masingha.999/employeemanagement_mean_stack/-/raw/main/snapps/Employee%20Create.PNG "Optional tittle")

## DB After Create Employee 

![DB after employee adding](https://gitlab.com/Masingha.999/employeemanagement_mean_stack/-/raw/main/snapps/Employee%20with%20Db.PNG "Optional tittle")

## Get all Employee Function

![Get all employee function](https://gitlab.com/Masingha.999/employeemanagement_mean_stack/-/raw/main/snapps/Get%20all%20employee.PNG "Optional tittle")

## License
Licence Under Maintainer (Naveen Masingha (2021))

