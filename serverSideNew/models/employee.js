const mongoose = require('mongoose');

var Employee = mongoose.model('Employee', {
    name: String ,
    position: String,
    salary: Number, 
    department: String,
    joinDate: Date
    
});

module.exports = { Employee };